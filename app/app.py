from cmath import log
from flask import Flask, request, jsonify
from flask_httpauth import HTTPBasicAuth

from numpy import double

from classes.score import confidence
from dataBase.queryDB import query_ca_us
from classes.suggestions import suggestion
from utilities.errorsAPI import errorsDict

app = Flask(__name__)
app.config['JSON_SORT_KEYS'] = False
auth = HTTPBasicAuth()

users = {
    "admin1": "YWRtaW4xcGFzcw==",
    "admin2": "YWRtaW4ycGFzcw=="
}

@auth.verify_password
def verify_password(username, password):
    if username in users and password == users.get(username):
        return username

@app.route('/healthcheck')
@auth.login_required
def index():
    return jsonify({"serviceStatus": "success"})

@app.route('/search')
@auth.login_required
def searching():
    #Read query params
    args = request.args
    q_string = args.get("q", default="", type=str)
    latitude = args.get("latitude", default=0, type=double)
    longitude = args.get("longitude", default=0, type=double)

    logger.info("Args: "+ "q: " + q_string + ", latitude: " + str(latitude) + ", longitude: " + str(longitude))

    #Data control errors
    if q_string == "":
        error_code = "err01"
        return jsonify({"errorCode": error_code, "message": errorsDict.get("err01")}), 400
    elif latitude != 0 and longitude == 0:
        error_code = "err02"
        return jsonify({"errorCode": error_code, "message": errorsDict.get("err02")}), 400
    elif latitude == 0 and longitude != 0:
        error_code = "err03"
        return jsonify({"errorCode": error_code, "message": errorsDict.get("err03")}), 400

    #Query the data base to get the suggestions
    q_result = query_ca_us(q_string)

    #Make response
    response = []
    for result in q_result:
        # Calculate the suggestion score
        sugg_score = 1 - confidence.suggestion_confidence(q_string, latitude, longitude, result[0], result[3], result[4], result[5])
        # Add new suggestion to the response array
        new_suggestion = suggestion(result[0], result[3], result[4], round(sugg_score,3), result[1], result[2])
        response.append(new_suggestion.make_suggestion())

    # Sort the array with the score in descenging order
    response.sort(key=lambda json: json['score'], reverse= True)
    response = {"search": response}

    return jsonify(response), 200

if __name__== '__main__':

    from waitress import serve
    import logging
    logging.basicConfig()

    logger = logging.getLogger('waitress')
    logger.setLevel(logging.INFO)
    logger.info("logger set to INFO")
    #app.run(debug=True, port=5025)
    serve(app, host='localhost', port= 5050)
