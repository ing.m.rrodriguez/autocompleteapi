import sqlite3
import pandas as pd

# load data
df = pd.read_csv('ca_us_large_cities.csv')

# strip whitespace from headers
df.columns = df.columns.str.strip()
con = sqlite3.connect("ca_us_cities.db")

# drop data into database
df.to_sql("cities", con)

con.commit()
con.close()