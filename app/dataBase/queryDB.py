import sqlite3
import os.path

def query_ca_us(q_string):

    BASE_DIR = os.path.dirname(os.path.abspath(__file__))
    db_path = os.path.join(BASE_DIR, "ca_us_cities.db")
    con = sqlite3.connect(db_path)
    cur = con.cursor()

    cur.execute(
        """SELECT name, "country code", "admin1 code", latitude, longitude, instr(name, :q)
        FROM cities 
        WHERE INSTR(name, :q) > 0;""", {"q": q_string}
        )

    con.commit()
    #print(cur.fetchall())
    query_cursor = cur.fetchall()
    print(query_cursor)
    con.close()
    return query_cursor
