
"""
Candian state codes
CA.01	Alberta	Alberta	5883102
CA.02	British Columbia	British Columbia	5909050
CA.03	Manitoba	Manitoba	6065171
CA.04	New Brunswick	New Brunswick	6087430
CA.05	Newfoundland and Labrador	Newfoundland and Labrador	6354959
CA.07	Nova Scotia	Nova Scotia	6091530
CA.08	Ontario	Ontario	6093943
CA.09	Prince Edward Island	Prince Edward Island	6113358
CA.10	Quebec	Quebec	6115047
CA.11	Saskatchewan	Saskatchewan	6141242
CA.12	Yukon	Yukon	6185811
CA.13	Northwest Territories	Northwest Territories	6091069
CA.14	Nunavut	Nunavut	6091732
"""
dict_codes = {
    1: 'AB', 
    2: 'BC', 
    3: 'MB', 
    4: 'NB', 
    5: 'NL', 
    7: 'NS', 
    8: 'ON', 
    9: 'PE', 
    10: 'QC', 
    11: 'SK', 
    12: 'YT', 
    13: 'NT', 
    14: 'NU' 
    }
