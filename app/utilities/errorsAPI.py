
# Errors dictionary

errorsDict = {
            "err01": "Query param 'q' is mandatory",
            "err02": "Query param 'longitude' is required to search by degrees distance",
            "err03": "Query param 'latitude' is required to search by degrees distance"
            }