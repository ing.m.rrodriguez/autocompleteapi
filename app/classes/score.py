# Calculate the confidence of the suggetions

from math import sqrt
from re import S

class confidence:

    def __init__(self):
        pass
    
    # Make calculations
    def suggestion_confidence(q_string, latitude, longitud, suggestion, s_latitude, s_longitude, position_ocurrence):
        if latitude == 0 or longitud == 0:
            sugg_score = confidence.query_unconfidence(q_string, suggestion) + confidence.caracter_unconfidence(position_ocurrence)
        else:
            sugg_score = confidence.query_unconfidence(q_string, suggestion) + confidence.caracter_unconfidence(position_ocurrence) + confidence.distance_unconfidence(latitude,longitud, s_latitude, s_longitude)
        # Put the limit score in .05
        if sugg_score > 1:
            sugg_score = .95

        return round(sugg_score,3)

    # Calculate the grade of unconfindence acording to the length difference
    def query_unconfidence(q_string, suggestion):
        string_difference = abs(len(suggestion) - len(q_string))
        unconfidence = string_difference*.05
        return unconfidence

    #Calculete the grade of unconfidence acording the distance using the distance between degrees
    def distance_unconfidence(latitude, longitude, s_latitude, s_logitude):
        distance_difference = sqrt(pow(latitude - s_latitude, 2) + pow(longitude - s_logitude, 2))
        return round(distance_difference*.02,3)

    #Adding unconfidence by the position of the occurrence
    def caracter_unconfidence(position_ocurrence):
        return (position_ocurrence - 1)*.05
