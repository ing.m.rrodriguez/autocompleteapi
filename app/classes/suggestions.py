from flask import jsonify
from utilities import canadianCodes

class suggestion:

    def __init__(self, name, latitude, longitude, score, country, state):
        self.name = name
        self.latitude = latitude
        self.longitude = longitude
        self.score = score
        self.country = country
        self.state = state

    def make_suggestion(self):
        
        # Generate standar codes for states and countries
        if self.country == 'CA':
            self.state = canadianCodes.dict_codes.get(int(self.state))
        else:
            self.country = 'USA'

        # Generate JSON objects
        data = {"name": self.name + ", " + self.state + ", " + self.country ,
                "latitude": self.latitude,
                "longitude": self.longitude,
                "score": self.score}
        
        return(data)
