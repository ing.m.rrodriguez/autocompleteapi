import unittest
import requests
import json
import os
import base64

class TestAPI (unittest.TestCase):

    @classmethod
    def setUpClass(self):
        """Set up function called when class is consructed."""
        self.base_url = 'http://127.0.0.1:5050/search'
        credentials = "admin1:YWRtaW4xcGFzcw=="
        credentials = credentials.encode('ascii')
        credentials = base64.b64encode(credentials)
        base64_credentials = credentials.decode('ascii')
        self.headers = {'Authorization': 'Basic ' + base64_credentials}
        self.file_json_path = os.path.dirname(os.path.abspath(__file__))

    def test_api_query_ok(self):
        response = requests.get(self.base_url, params= {"q":"Boston"}, headers= self.headers)
        jsonRes = response.json()

        file_json_path = self.file_json_path + '/jsonFiles/searchOk.json'

        try:
            with open(file_json_path) as fd:
                contentJson = json.load(fd)
        except IOError:
            print('exception while opening a file %s\n' % (file_json_path))

        print(contentJson)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonRes, contentJson)

    def test_api_query_error(self):
        response = requests.get(self.base_url, headers= self.headers)
        jsonRes = response.json()

        file_json_path = self.file_json_path + '/jsonFiles/searchError.json'

        try:
            with open(file_json_path) as fd:
                contentJson = json.load(fd)
        except IOError:
            print('exception while opening a file %s\n' % (file_json_path))

        self.assertEqual(response.status_code, 400)
        self.assertEqual(jsonRes, contentJson)

    def test_api_query_degrees_ok(self):
        response = requests.get(self.base_url, params= {"q":"Monica", "longitude": "-116.15269023746764", "latitude": "34.39571383649506"}, headers= self.headers)
        jsonRes = response.json()

        file_json_path = self.file_json_path + '/jsonFiles/searchOkDegrees.json'

        try:
            with open(file_json_path) as fd:
                contentJson = json.load(fd)
        except IOError:
            print('exception while opening a file %s\n' % (file_json_path))

        print(contentJson)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(jsonRes, contentJson)

    def test_api_query_degrees_error(self):
        response = requests.get(self.base_url, params= {"q":"Bosto", "longitude": "-79.4163"}, headers= self.headers)
        jsonRes = response.json()

        file_json_path = self.file_json_path + '/jsonFiles/searchErrorDegrees.json'

        try:
            with open(file_json_path) as fd:
                contentJson = json.load(fd)
        except IOError:
            print('exception while opening a file %s\n' % (file_json_path))

        self.assertEqual(response.status_code, 400)
        self.assertEqual(jsonRes, contentJson)

if __name__ == '__main__':
    # Test api endpoints
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAPI)
    unittest.TextTestRunner(verbosity=2).run(suite)