# autocompleteAPI

The purpose of this repository is a code challenge where you can search from de largest cities of USA and Canada, autocompleting the string that you query.

# Requirements

`pip install -r requirements.txt`

The following Python packages will be installed:

click (8.0.4)
colorama (0.4.4)
Flask (2.0.3)
Flask-HTTPAuth (4.5.0)
itsdangerous (2.1.0)
Jinja2 (3.0.3)
MarkupSafe (2.1.0)
numpy (1.22.2)
waitress (2.0.0)
Werkzeug (2.0.3)
nose2 (0.11.0)

# Run Test Cases
To execute all test cases, change directory to the project root folder and execute the following command:

`nose2 -v`
